$(bootstrap);

/**
 * Método que se ejecuta al iniciar el proceso
 */
function bootstrap() {

  // Ubicación de la UNGS.
  var ungsLocation = [-34.5335003, -58.7011877];

  // Creación del componente mapa de Leaflet.
  var map = L.map('mapid').setView(ungsLocation, 14);

  // Agregamos los Layers de OpenStreetMap.
  var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  // Agregamos el control para seleccionar Layers al mapa
  var layersControl = L.control.layers({
    "Base": baseLayer
  });
  layersControl.addTo(map);
  // hack:
  map.layersControl = layersControl;

  agregarMoviles(map)
  agregarCentrosAsistencia(map)
}

var centerIcon = L.icon({
  iconUrl: 'img/centro.png',
  iconSize: [40, 40],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76]
});

/**
 * Agrega los centros de asistencia al mapa
 * @param {Mapa donde se mostrarán los centros} map 
 */
function agregarCentrosAsistencia(map) {

  //Creamos un grupo de puntos...
  let centrosLayer = L.layerGroup().addTo(map);
  // Agregamos la capa al control del mapa
  map.layersControl.addOverlay(centrosLayer, "Centros");

  var centros = lugares;
  for (i in centros.Lugares) {
    let maker = L.marker([centros.Lugares[i].Latitud, centros.Lugares[i].Longitud], {
      icon: centerIcon
    }).bindPopup(`${centros.Lugares[i].nombre} <br />Dirección: ${centros.Lugares[i].direccion} <br /> Telefono: ${centros.Lugares[i].telefono}`);
    centrosLayer.addLayer(maker)

    $("#centros").append(`<li class="list-group-item"> ${centros.Lugares[i].nombre}<br/>
      ${centros.Lugares[i].direccion}<br/>
      ${centros.Lugares[i].telefono}<br/>
      </li>`)
  }

}

/**
 * Agrega los móviles recibidos en un mapa
 * @param {mapa donde se mostrarán los móviles} map 
 */
function agregarMoviles(map) {
  //Creamos un grupo de puntos...
  var movilLayer = L.layerGroup().addTo(map);
  // Agregamos la capa al control del mapa
  map.layersControl.addOverlay(movilLayer, "Moviles");

  //Consultamos los móviles
  axios.get(`${Config.herokuApi}/supporttrucks/`)
    .then(function (response) {
      response.data.supportTrucks.forEach(truck => {
        mover(truck.id, movilLayer)
      });
    })

}

/**
 * Funcion para mover el móvil
 * @param {Identificador del movil a mover} movilId 
 * @param {Capa por donde se moverá el móvil} movilLayer 
 */
async function mover(movilId, movilLayer) {
  //Obtenemos las posiciones del movil
  axios.get(`${Config.herokuApi}/supporttrucks/${movilId}/positions`)
    .then(function (response) {

      //Cuando el móvil deba moverse, se ejecutará esta acción
      var updater = function (newPosition) {
        console.log("Updating view for movil: " + movil.id + "!!");

        //Necesitamos que el estado se actualice
        const stateURL = `${Config.herokuApi}/truckstates/` + newPosition.state
        axios.get(stateURL).then((response) => {
          movil.openPopup = movil.layer != null && movil.layer.getPopup().isOpen()
          //Borramos el móvil de la capa de móviles
          if (movil.layer) {
            movilLayer.removeLayer(movil.layer)
          }

          //Agregamos el estilo del auto y del listado según el estado
          let carIconColor = ''
          let classEstado = 'list-group-item-success'
          if (response.data.state.id == 0) {
            classEstado = "list-group-item-success"
            carIconColor = './img/car3.png'
          } else if (response.data.state.id == 1) {
            classEstado = "list-group-item-info"
            carIconColor = './img/car.png'
          } else if (response.data.state.id == 2) {
            classEstado = "list-group-item-danger"
            carIconColor = './img/car2.png'
          }

          var carIcon = obtenerMovil(carIconColor);
          //Agregamos el marcador y mostramos el vehículo
          movil.layer = L.marker(newPosition.position, { icon: carIcon })
            .bindPopup(`ID: ${movil.id} <br />Estado: <b>${response.data.state.description}</b>`)
          movilLayer.addLayer(movil.layer);
          if (movil.openPopup) {
            console.log('Popup abierto')
            movil.layer.openPopup()
          }
          cargarMovil(movil, classEstado, response);
        }).catch((err) => console.log(err))
      }
      let movil = new Movil(response.data.truck_id, response.data.positions)
      $("#estadoMoviles").append(`<li class="list-group-item " id="movil${movil.id}"></li >`)

      movil.run(updater);
    })
    .catch(function (error) { });
}

/**
 * Renderiza un auto dependiendo el color
 * @param {Icono a renderizar} carIconColor 
 */
function obtenerMovil(carIconColor) {
  return L.icon({
    iconUrl: carIconColor,
    iconSize: [50, 50],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76]
  });
}

/**
 * Cambia el color del movil para poder identificar su estado
 * @param {Movil a indentificar} movil 
 * @param {Nueva clase para mostrar el estado} classEstado 
 * @param {Respuesta que identifica el estado} res 
 */
function cargarMovil(movil, classEstado, res) {
  $(`#movil${movil.id}`).empty();
  $(`#movil${movil.id}`).removeClass("list-group-item-success");
  $(`#movil${movil.id}`).removeClass("list-group-item-info");
  $(`#movil${movil.id}`).removeClass("list-group-item-danger");
  $(`#movil${movil.id}`).addClass(classEstado);
  $(`#movil${movil.id}`).append(`${movil.id} - Estado: ${res.data.state.description} `);
}
