/**
 * Método para cargar las alertas
 */
function cargarAlertas() {
    let id = 0

    //Obtenemos las alertas generales y posteriormente las mostramos
    axios.get(`${Config.smn}/alerts/type/AL`)
        .then(function (response) {
            response.data.forEach(element => {
                this.escribirAlerta(element, ++id, '#alertasGenerales')
            });
        })
        .catch((error) => {
            console.log(error)
        });

    getAlertasLocales($("#dias").val())
}

/**
 * Se buscan alertas una cantidad de días previos
 */
getAlertasLocales = (dias) => {
    let id = 10;
    //Se obtienen las alertas locales
    axios.get(`${Config.herokuApi}/alerts/day/` + dias)
        .then(function (response) {
            //Por cada alerta la escribimos en un listado
            response.data.alerts.forEach(element => {
                this.escribirAlerta(element, ++id, '#alertasLocales')
            });
        })
        .catch((error) => {
            console.log(error)
        });
}

/**
 * Se escriben alertas generales o locales en general
 * @param {respuesta del servicio de alertas locales o generales} element 
 * @param {id para cambiar el numero de item en la lista a mostrar} id 
 * @param {Bloque donde se cargaran dependiendo si son generales o locales} block 
 */
function escribirAlerta(element, id, block) {
    let card = `<div class="card">
                <div class="card-header" id="heading${id}">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#collapse${id}" aria-expanded="false" aria-controls="collapse${id}">
                            ${element.title} - ${element.date.split('-').reverse().join('/')}
                        </button>
                    </h2>
                </div>
                <div id="collapse${id}" class="collapse" aria-labelledby="heading${id}" data-parent="#alertasLocales">
                    <div class="card-body"> 
                        <span class="badge badge-pill badge-light">${element.hour} hs</span><br />
                        ${element.description}
                        <br /><b>Zonas afectadas</b>
                        <ul>
                            ${obtenerZonas(element.zones)}
                        </ul>
                    </div>
                </div>
            </div>`
    $(block).append(card)
}

/**
 * Se empieza a llenar la lista de zonas afectadas
 */
obtenerZonas = (zones) => {
    let zonasAfectadas = ""
    let i = 0
    let zona = zones[i]
    while (zona != undefined) {
        console.log(zona)
        zonasAfectadas = zonasAfectadas + `<li>${zona}</li>`
        i = i + 1
        zona = zones[i]
    }
    return zonasAfectadas
}

$(document).ready(function () {
    $(cargarAlertas)
});

/**
 * Al cambiar el día, se buscan nuevamente las alertas
 */
function buscarAlertas() {
    $('#alertasLocales').empty()
    getAlertasLocales($("#dias").val())
}