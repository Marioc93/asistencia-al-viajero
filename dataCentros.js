var lugares = {
    "Lugares": [
      { "nombre": "Comisaria 1era De San Miguel", "Latitud": -34.5414148, "Longitud": -58.7169633, "direccion": "Leandro N. Alem 1857, B1663GDP San Miguel, Buenos Aires", "telefono": "4664-9333" },
      { "nombre": "Bomberos Voluntarios de San Miguel", "Latitud": -34.5415174, "Longitud": -58.712632, "direccion": "Av. Pres. Juan Domingo Perón 1644, B1663GHS Gran Buenos Aires, Buenos Aires", "telefono": "011 4664-2222" },
      { "nombre": "Hospital Polo Malvinas", "Latitud": -34.5211837, "Longitud": -58.7190995, "direccion": "Av. Pres. Arturo Umberto Illia 32500, Los Polvorines, Buenos Aires", "telefono": "011 4469-9600" },
      { "nombre": "Clínica Neuropsiquiátrica San Miguel", "Latitud": -34.5161125, "Longitud": -58.7078905, "direccion": "Darragueira 962, San Miguel, Buenos Aires", "telefono": "011 4663-9270" },
      { "nombre": "Hospital Mercante", "Latitud": -34.5167676, "Longitud": -58.74698, "direccion": "Coronel Arias 874, José C. Paz, Buenos Aires", "telefono": "02320 44-0030" },
      { "nombre": "Maternidad Municipal María Eva Duarte de Perón", "Latitud": -34.4983427, "Longitud": -58.7148364, "direccion": "Av. del Sesquicentenario 1200, B1615LRM Pablo Nogués, Buenos Aires", "telefono": "011 4469-9600" },
      { "nombre": "Asociacion Bomberos Voluntarios Malvinas Argentina", "Latitud": -34.4979993, "Longitud": -58.6949157, "direccion": "Baroni 2684, B1613FCB Los Polvorines, Buenos Aires", "telefono": "011 4660-2222" },
      { "nombre": "Comisaria 2da.los polvorines", "Latitud": -34.4988462, "Longitud": -58.6893498, "direccion": "B1613GBE, Perito Moreno 3486, B1613GBE Villa de Mayo, Buenos Aires", "telefono": "011 5457-2003" },
    ]
  };