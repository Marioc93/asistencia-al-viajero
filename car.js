let Movil = function (id, historyPositions) {
    this.id = id;
    this.historyPositions = historyPositions;
    this.openPopup = false
    this.layer = null

    var actualIx = 0;

    this.run = function (callback) {
        var self = this;
        setTimeout(function () {
            callback(historyPositions[actualIx]);

            actualIx += 1;
            if (actualIx < historyPositions.length) {
                self.run(callback);
            }
        }, 1000);
    }
};